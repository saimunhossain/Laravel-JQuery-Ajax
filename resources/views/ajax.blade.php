<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">


    <title>Hello, world!</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Ajax CRUD</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">About</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Title</td>
                    <td>Details</td>
                    <td>Author</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="col-md-4">
            <form action="">
                <div class="form-group myid">
                    <label for="name">ID</label>
                    <input type="number" id="id" name="id" class="form-control" placeholder="Type Title">
                </div>
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" id="title" name="title" class="form-control" placeholder="Type Title">
                </div>
                <div class="form-group">
                    <label for="details">Details</label>
                    <textarea id="details" name="derails" class="form-control" rows="2" placeholder="Type Details"></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Author</label>
                    <input type="text" id="author" class="form-control" name="author" placeholder="Type Title">
                </div>
                <button type="submit" id="save" onclick="saveData()" class="btn btn-success">Save</button>
                <button type="submit" id="update" onclick="updateData()" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#datatable').DataTable();
    $('#save').show();
    $('#update').hide();
    $('#myid').hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function viewData() {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/post",
            success: function(response) {
                var rows = "";
                $.each(response, function (key, value) {
                    rows = rows +"<tr>";
                    rows = rows +"<td>"+value.id+"</td>";
                    rows = rows +"<td>"+value.title+"</td>";
                    rows = rows +"<td>"+value.details+"</td>";
                    rows = rows +"<td>"+value.author+"</td>";
                    rows = rows +"<td>";
                    rows = rows +"<button type='button' onclick='editData("+value.id+","+value.name+","+value.details+","+value.author+")'></button>";
                    rows = rows +"<button type='button' onclick='deleteData("+value.id+")'></button>";
                    rows = rows +"</td></tr>"
                });
                $('tbody').html(rows);
            }
        })
    }
    viewData();
    function saveData() {

    }
    function clearData() {
        $('#id').val('');
        $('#title').val('');
        $('#details').val('');
        $('#author').val('');
    }
    function editData(id, name, details, athor) {

    }
    function updateData() {

    }
    function deleteData(id) {

    }
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>